﻿using System.Data.Entity;

namespace Problem3
{
    /* Problem
     * You have a table with some frequently used fields and a few large, but rarely needed fields.
     * For performance reasons, you want to avoid needlessly loading these expensive fields on every query.
     * You want to split the table across two or more entities.
     */
    public class P7Entities : DbContext
    {
        public virtual DbSet<Photograph> Photographs { get; set; }
        public virtual DbSet<PhotographFullImage> PhotographFullImages { get; set; }

        public P7Entities(): base("name=P7CodeFirstContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Photograph>()
                .HasRequired(p => p.PhotographFullImage)
                .WithRequiredPrincipal(p => p.Photograph);
            modelBuilder.Entity<Photograph>().ToTable("Photograph", "dbo");
            modelBuilder.Entity<PhotographFullImage>().ToTable("Photograph", "dbo");
        }
    }
}
